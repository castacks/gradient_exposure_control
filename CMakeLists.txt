cmake_minimum_required(VERSION 2.8.3)
project(gradient_exposure_control)

set(CMAKE_CXX_FLAGS "-std=c++11 ${CMAKE_CXX_FLAGS}")

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  cmake_modules
  roscpp
  sensor_msgs
  #visualization_msgs
  #tf2
)

#find_package(Eigen REQUIRED)
#find_package(PCL REQUIRED COMPONENTS common io)
find_package(OpenCV 2.4 REQUIRED HINTS ~/Software/opencv_2.4.9.1)
#find_package(Boost REQUIRED)
#find_package(NUMPY REQUIRED)

#catkin_python_setup()

catkin_package(
  INCLUDE_DIRS include
  LIBRARIES gradient_exposure_control
  CATKIN_DEPENDS roscpp sensor_msgs
  DEPENDS opencv
)

###########
## Build ##
###########

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

add_library(${PROJECT_NAME} src/gradient_exposure_control.cpp)
target_link_libraries(${PROJECT_NAME} ${catkin_LIBRARIES} ${OpenCV_LIBRARIES})

add_executable(runonce scratch/runonce.cpp)
target_link_libraries(runonce ${catkin_LIBRARIES} ${OpenCV_LIBRARIES} ${PROJECT_NAME})

#target_link_libraries(${PY_PROJECT_NAME}
#    ${catkin_LIBRARIES}
#    ${Boost_LIBRARIES}
#)
