#include "gradient_exposure_control/gradient_exposure_control.h"

#include <boost/math/tools/minima.hpp>

#include <ros/ros.h>
#include <ros/console.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

namespace ca { namespace gradient_exposure_control {

struct GammaFn {
  cv::Mat_<float> img_;
  cv::Mat_<float> img2_;
  GradientExposureController* obj_;
  GammaFn(const cv::Mat_<float>& img, GradientExposureController* obj) : img_(img), obj_(obj)
  { }
  float operator()(float gamma) {
    std::cout << "call" << std::endl;
    cv::pow(img_, gamma, img2_);
    float y = obj_->ComputeQualityMetric(img2_);
    return -y;
  }
};

void GradientExposureController::GradientMagnitude(const cv::Mat_<float>& img, cv::Mat_<float>& dst) {
  // TODO maybe smooth before, or use scharr
  // TODO beware scaling 0,1
  cv::Mat_<float> gx, gy, gx2, gy2, sumgx2gy2;
  // the scale normalizes the sobel filter to unit weight
  cv::Sobel(img, gx, CV_32F, 1, 0, 3, 1./4.0);
  cv::Sobel(img, gy, CV_32F, 0, 1, 3, 1./4.0);
  cv::pow(gx, 2.0, gx2);
  cv::pow(gy, 2.0, gy2);
  sumgx2gy2 = gx2 + gy2;
  cv::sqrt(sumgx2gy2, dst);

}

void GradientExposureController::MappedGradientMagnitude(const cv::Mat_<float>& gradmag, cv::Mat_<float>& dst) {
  float N = log(params_.lambda * (1.-params_.delta) + 1.);
  dst.create(gradmag.rows, gradmag.cols);
  // TODO verify isContinuous
  int px  = gradmag.rows*gradmag.cols;
  const float* data_in = gradmag.ptr<float>();
  float* data_out = dst.ptr<float>();
  for (int i=0; i < px; ++i) {
    float mi = data_in[i];
    if (mi < params_.delta) {
      data_out[i] = 0.;
      continue;
    }
    float logarg = params_.lambda*(mi-params_.delta)+1.;
    if (logarg < 0.) {
      data_out[i] = 0.;
      continue;
    }
    data_out[i] = log(logarg)/N;
    //if (std::isnan(data_out[i])) { ROS_ERROR_STREAM("data_out nan, logarg: " << logarg << ", N: " << N << ", mi: " << mi); }
  }
}

float GradientExposureController::ComputeQualityMetric(const cv::Mat_<float>& img) {
  this->GradientMagnitude(img, m_);
  this->MappedGradientMagnitude(m_, mbar_);

  //cv::namedWindow("win", cv::WINDOW_AUTOSIZE);cv::imshow( "win", m_ ); cv::waitKey(10);
  //cv::namedWindow("win", cv::WINDOW_AUTOSIZE);cv::imshow( "win", mbar_ ); cv::waitKey(10);

  float M(-1.);

  if (params_.margin_rows > 0 || params_.margin_cols > 0) {
    // mask out borders to focus on center. TODO use opencv.
    int rows2 = mbar_.rows - params_.margin_rows;
    int cols2 = mbar_.cols - params_.margin_cols;
    M = 0.;
    for (int r=params_.margin_rows; r < rows2; ++r) {
      for (int c=params_.margin_cols; c < cols2; ++c) {
        M += mbar_(r, c);
      }
    }
  } else {
    M = cv::sum(mbar_)[0];
  }

  return M;
}

#if 0
float GradientExposureController::ComputeGammaHat(const cv::Mat_<float>& img) {
  // TODO this step is naive and slow atm
  //std::array<float, 7> gammas{ { 0.1, 0.5, 0.8, 1.0, 1.2, 1.5, 1.9 } };
  //std::array<float, 7> qualities { };
  cv::Mat_<float> img2;
  float quality_argmax = 0.;
  float quality_max = -1.;
  //for (size_t i=0; i < gammas.size(); ++i) {
  const float gamma_min = 0.05;
  const float gamma_max = 2.0;
  int increments = 9;
  float dx = (gamma_max-gamma_min)/increments;
  for (int i=0; i < increments; ++i) {
    //float gamma = gammas[i];
    float gamma = gamma_min + dx*i;
    cv::pow(img, gamma, img2);
    float q = this->ComputeQualityMetric(img2);
    //ROS_INFO_STREAM("gamma: " << gamma << " q: " << q);
    if (q > quality_max) {
      quality_max = q;
      quality_argmax = gamma;
    }
  }
  std::cout << "argmax " << quality_argmax << "\n";
  return quality_argmax;
}
#endif

#if 0
float GradientExposureController::ComputeGammaHat(const cv::Mat_<float>& img) {
  GammaFn fn(img, this);
  float gamma_min = 0.05;
  float gamma_max = 2.0;
  std::pair<float, float> out = boost::math::tools::brent_find_minima(fn, gamma_min, gamma_max, 8);
  //std::pair<float, float> out = boost::math::tools::brent_find_minima(fn, gamma_min, gamma_max, 8);
  //ROS_INFO_STREAM("argmax = " << out.first << ", max  = " << out.second);
  std::cout << "argmax " << out.first << "\n";
  return out.first;
}
#endif

#if 1
float GradientExposureController::ComputeGammaHat(const cv::Mat_<float>& img) {
  GammaFn fn(img, this);
  float quality_argmax = 0.;
  float quality_max = -1.;
  const float gamma_min = 0.05;
  const float gamma_max = 2.0;
  int increments = 9;
  float dx = (gamma_max-gamma_min)/increments;
  for (int i=0; i < increments; ++i) {
    float gamma = gamma_min + dx*i;
    float q = -fn(gamma);
    //ROS_INFO_STREAM("gamma: " << gamma << " q: " << q);
    if (q > quality_max) {
      quality_max = q;
      quality_argmax = gamma;
    }
  }
  std::cout << "argmax " << quality_argmax << "\n";
  return quality_argmax;
}
#endif


float GradientExposureController::ComputeNewExposure(float old_exposure, const cv::Mat_<float>& img) {
  // TODO limits based on reasonable gain/shutter
  // TODO figure out int value to time mapping
  float gamma_hat = this->ComputeGammaHat(img);
  //ROS_INFO_STREAM("gamma_hat = " << gamma_hat);
  float alpha = (gamma_hat < 1.)? 1. : 0.5;
  float err = (1. - gamma_hat);
  float new_exposure = old_exposure*(1. + alpha * params_.kp * err);
  //clamp
  new_exposure = std::min(1.f, std::max(0.f, new_exposure));
  ROS_INFO_STREAM("exposure estimate = " << (-err) << ", new scaled exposure = " << new_exposure);

#if 0
  pub_gamma_error_.publish(errmsg);
  pub_exposure_float_.publish(expmsg);
#endif
#if 0
  static int seq = 0;
  geometry_msgs::PointStamped expmsg;
  expmsg.header.stamp = ros::Time::now();
  expmsg.header.seq = seq++;
  expmsg.point.x = err;
  expmsg.point.y = new_exposure;
  pub_exposure_diag_.publish(expmsg);
#endif
  return new_exposure;

}

} /* gradient_exposure_control */
} /* ca */
