/**
 * @author  Daniel Maturana
 * @year    2015
 *
 * @attention Copyright (c) 2015
 * @attention Carnegie Mellon University
 * @attention All rights reserved.
 */

#ifndef _GRADIENT_EXPOSURE_CONTROL_GRADIENT_EXPOSURE_CONTROL_H_
#define _GRADIENT_EXPOSURE_CONTROL_GRADIENT_EXPOSURE_CONTROL_H_

#include <ros/ros.h>
#include <opencv2/core/core.hpp>

namespace ca { namespace gradient_exposure_control {

struct Parameters {
  double delta; ///< gradients smaller than this are ignored
  double lambda; ///< controls log-mapping of gradient magnitude
  double max_exposure; ///< exposure hard limit
  double min_exposure; ///< exposure hard limit
  int margin_rows; ///< rows to exclude margin
  int margin_cols; ///< cols to exclude margin
  double kp; ///< proportional controller parameter

  Parameters() :
      delta ( 0.11 ),
      lambda ( 20. ),
      max_exposure ( 1.0 ),
      min_exposure ( 0.0 ),
      margin_rows ( 0 ),
      margin_cols ( 0 ),
      kp ( 0.2 )
  {
  }

  void SetFromNodeHandle(const ros::NodeHandle& nh) {
    if (!nh.getParam("delta", delta)) {
      delta = 0.11;
    }
    if (!nh.getParam("lambda", lambda)) {
      lambda = 20.;
    }
    if (!nh.getParam("max_exposure", max_exposure)) {
      max_exposure = 1.0;
    }
    if (!nh.getParam("min_exposure", min_exposure)) {
      min_exposure = 0.0;
    }
    if (!nh.getParam("margin_rows", margin_rows)) {
      margin_rows = 0;
    }
    if (!nh.getParam("margin_cols", margin_cols)) {
      margin_cols = 0;
    }
    if (!nh.getParam("kp", kp)) {
      kp = 0.2;
    }
  }
};

class GradientExposureController {
public:
  GradientExposureController() {
    params_ = Parameters();
  }
  GradientExposureController(const Parameters& params) {
    params_ = params;
  }
  virtual ~GradientExposureController() { }

private:
  GradientExposureController(const GradientExposureController& other);
  GradientExposureController& operator=(const GradientExposureController& other);

public:
  float ComputeNewExposure(float old_exposure, const cv::Mat_<float>& img);

  void GradientMagnitude(const cv::Mat_<float>& img, cv::Mat_<float>& dst);
  void MappedGradientMagnitude(const cv::Mat_<float>& gradmag, cv::Mat_<float>& dst);
  float ComputeQualityMetric(const cv::Mat_<float>& img);
  float ComputeGammaHat(const cv::Mat_<float>& img);

public:
  Parameters params_;

private:
  cv::Mat_<float> m_, mbar_;

};

} /* gradient_exposure_control */
} /* ca */

#endif
